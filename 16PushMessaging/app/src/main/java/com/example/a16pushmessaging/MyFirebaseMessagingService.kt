package com.example.a16pushmessaging

import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {

    var broadcast = LocalBroadcastManager.getInstance(this)

    override fun onMessageReceived(message: RemoteMessage?) {
        sendBroadcast(message?.notification?.body)
    }

    private fun sendBroadcast(message: String?) {
        val intent = Intent("MyMessage")
        intent.putExtra("message", message)
        broadcast.sendBroadcast(intent)
    }
}