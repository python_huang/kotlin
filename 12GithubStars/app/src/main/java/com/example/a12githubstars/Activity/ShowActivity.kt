package com.example.a12githubstars.Activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.example.a12githubstars.Adapter
import com.example.a12githubstars.Data.ReposData
import com.example.a12githubstars.Data.UserData
import com.example.a12githubstars.R
import kotlinx.android.synthetic.main.show_layout.*

class ShowActivity : AppCompatActivity() {
    lateinit var userData: UserData
    lateinit var reposList: ArrayList<ReposData>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.show_layout)
        getData()
        setUserView()
        initRecyclerView()
    }

    private fun getData() {
        userData = intent.extras?.get("UserData") as UserData
        reposList = intent.getParcelableArrayListExtra("ReposDataList")
    }

    private fun setUserView() {
        avatar.setImageBitmap(userData.avatar)
        account.text = userData.account
        name.text = userData.userName
        reposNum.text = userData.reposCount
        followersNum.text = userData.followers
        followingNum.text = userData.following
    }

    private fun initRecyclerView() {
        recyclewView.adapter = Adapter(this, reposList)
        recyclewView.layoutManager = LinearLayoutManager(this)
    }

}