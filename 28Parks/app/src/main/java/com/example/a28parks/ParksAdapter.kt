package com.example.a28parks

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class ParksAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> Park1Fragment()
            1 -> Park2Fragment()
            2 -> Park3Fragment()
            3 -> Park4Fragment()
            4 -> Park5Fragment()
            else -> Park6Fragment()

        }

    }

    override fun getCount(): Int {
        return 6
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Park 1"
            1 -> "Park 2"
            2 -> "Park 3"
            3 -> "Park 4"
            4 -> "Park 5"
            else -> "Park 6"

        }
    }

}