package com.example.a6imagelist

import android.content.Context

class content(val image: Int, val text: String)

var list = mutableListOf<content>()

class SetData {
    fun setList(context: Context): MutableList<content> {
        for (i in 1..15) {
            val name = "img_$i"
            val id = context.resources.getIdentifier(name, "drawable", context.packageName)
            list.add(content(id, name))
        }
        return list
    }
}