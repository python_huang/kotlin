package com.example.a10progresscontrol

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var currentProgress: Int = 0
    private var maxProgress = 100
    private var repeatTaskTime: Long = 500

    private var taskHandler = Handler()
    private var runnable = Runnable { startIncreaseProgress() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupView()
    }

    private fun setupView() {
        startButton.setOnClickListener(startButtonHandler)
        pauseButton.setOnClickListener(pauseButtonHandler)
        stopButton.setOnClickListener(stopbuttonHandler)

        // setup progress
        progressBar.max = 100
        progressBar.progress = 0

    }

    private var startButtonHandler = View.OnClickListener {
        startIncreaseProgress()
    }

    private var pauseButtonHandler = View.OnClickListener {
        pauseIncreasingProgress()
    }

    private var stopbuttonHandler = View.OnClickListener {
        resetProgress()
    }

    private fun startIncreaseProgress() {
        if (currentProgress >= maxProgress) {
            pauseIncreasingProgress()
            return
        } else {
            increaseProgressBy()
        }

        println("post runnable")
        taskHandler.postDelayed(runnable, repeatTaskTime)

    }

    private fun increaseProgressBy() {
        println("remove task handler")

        currentProgress += 10
        progressBar.progress = currentProgress

        updateProgressTextView()
    }

    private fun pauseIncreasingProgress() {
        println("remove task handler")

        taskHandler.removeCallbacksAndMessages(null)
    }

    private fun resetProgress() {
        println("reset progress")

        pauseIncreasingProgress()

        currentProgress = 0
        progressBar.progress = 0

        updateProgressTextView()
    }

    private fun updateProgressTextView() {
        progressTextView.text = "$currentProgress%"
    }

}